"""empty message

Revision ID: 94afb9aee5e1
Revises: 34be8b8cdc4a
Create Date: 2018-04-16 00:51:59.892000

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '94afb9aee5e1'
down_revision = '34be8b8cdc4a'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('bimages',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('im_banner', sa.BLOB(), nullable=True),
    sa.Column('is_banner', sa.Boolean(), nullable=True),
    sa.Column('name', sa.String(length=60), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('bimages')
    # ### end Alembic commands ###
