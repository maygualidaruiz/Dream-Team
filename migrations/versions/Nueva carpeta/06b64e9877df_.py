"""empty message

Revision ID: 06b64e9877df
Revises: 6c46ee4ebfea
Create Date: 2018-04-15 21:23:03.095000

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '06b64e9877df'
down_revision = '6c46ee4ebfea'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('images',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('im_banner', sa.BLOB(), nullable=True),
    sa.Column('is_banner', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('im_banner')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('images')
    # ### end Alembic commands ###
