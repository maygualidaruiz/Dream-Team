# app/__init__.py

# third-party imports
from flask import abort, Flask, render_template
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy


import smtplib
import config
import shutil

# local imports
from config import app_config
from email import login_email, password_email
import app
from PIL.JpegImagePlugin import APP




db = SQLAlchemy()
login_manager = LoginManager()

def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    Bootstrap(app)
    db.init_app(app)
    login_manager.init_app(app)
    login_manager.login_message = "You must be logged in to access this page."
    login_manager.login_view = "auth.login"
    migrate = Migrate(app, db)

    from app import models

    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .home import home as home_blueprint
    app.register_blueprint(home_blueprint)
    
    @app.errorhandler(403)
    def forbidden(error):
        return render_template('errors/403.html', title='Forbidden'), 403

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('errors/404.html', title='Page Not Found'), 404

    @app.errorhandler(500)
    def internal_server_error(error):
        return render_template('errors/500.html', title='Server Error'), 500

    @app.route('/500')
    def error():
        abort(500)
        
    return app

def send_email(tipo_email,dest_nombre,dest_email):

    if tipo_email=="1":
       subject = "Bienvenida"
       msg = "Hola " + dest_nombre.strip() + " bienvenid@ a nuestro grupo de trabajo"

    login=login_email
    password=password_email
    
    destinatario = dest_email.strip()
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.ehlo()
    server.starttls()
    server.login(login, password)
        
    message = 'Subject: {}\n\n{}'.format(subject, msg)
    server.sendmail(login, destinatario, message)
    server.quit()
 
    
    return app

def actualiza_imagen(list_images,origen):
    
    
    for o in list_images:
        o.is_banner = False
        db.session.add(o)
        db.session.commit()
    
    file_oriegen=origen
    file_destino='../dream-team/app/static/img/intro-bg.jpg'
    shutil.copyfile(file_oriegen,file_destino)

                
    return app