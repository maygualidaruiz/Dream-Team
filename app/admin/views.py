# app/admin/views.py

from flask import abort, flash, redirect, render_template, url_for
from flask_login import current_user, login_required
from ..models import Department, Employee, Role, BImage


from . import admin
from forms import DepartmentForm, EmployeeAssignForm, RoleForm, ImageForm
from .. import db
from _mysql import escape, escape_string
from string import strip
from app.models import BImage
from app import actualiza_imagen

import os.path as path

def check_admin():
    """
    Prevent non-admins from accessing the page
    """
    if not current_user.is_admin:
        abort(403)

# Department Views

@admin.route('/departments', methods=['GET', 'POST'])
@login_required
def list_departments():
    """
    List all departments
    """
    check_admin()

    departments = Department.query.all()

    return render_template('admin/departments/departments.html',
                           departments=departments, title="Departments")

@admin.route('/departments/add', methods=['GET', 'POST'])
@login_required
def add_department():
    """
    Add a department to the database
    """
    check_admin()

    add_department = True

    form = DepartmentForm()
    if form.validate_on_submit():
        department = Department(name=form.name.data,
                                description=form.description.data)
        try:
            # add department to the database
            db.session.add(department)
            db.session.commit()
            flash('You have successfully added a new department.')
        except:
            # in case department name already exists
            flash('Error: department name already exists.')

        # redirect to departments page
        return redirect(url_for('admin.list_departments'))

    # load department template
    return render_template('admin/departments/department.html', action="Add",
                           add_department=add_department, form=form,
                           title="Add Department")

@admin.route('/departments/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_department(id):
    """
    Edit a department
    """
    check_admin()

    add_department = False

    department = Department.query.get_or_404(id)
    form = DepartmentForm(obj=department)
    if form.validate_on_submit():
        department.name = form.name.data
        department.description = form.description.data
        db.session.commit()
        flash('You have successfully edited the department.')

        # redirect to the departments page
        return redirect(url_for('admin.list_departments'))

    form.description.data = department.description
    form.name.data = department.name
    return render_template('admin/departments/department.html', action="Edit",
                           add_department=add_department, form=form,
                           department=department, title="Edit Department")

@admin.route('/departments/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_department(id):
    """
    Delete a department from the database
    """
    check_admin()

    department = Department.query.get_or_404(id)
    db.session.delete(department)
    db.session.commit()
    flash('You have successfully deleted the department.')

    # redirect to the departments page
    return redirect(url_for('admin.list_departments'))

    return render_template(title="Delete Department")



# Role Views

@admin.route('/roles')
@login_required
def list_roles():
    check_admin()
    """
    List all roles
    """
    roles = Role.query.all()
    return render_template('admin/roles/roles.html',
                           roles=roles, title='Roles')

@admin.route('/roles/add', methods=['GET', 'POST'])
@login_required
def add_role():
    """
    Add a role to the database
    """
    check_admin()

    add_role = True

    form = RoleForm()
    if form.validate_on_submit():
        role = Role(name=form.name.data,
                    description=form.description.data)

        try:
            # add role to the database
            db.session.add(role)
            db.session.commit()
            flash('You have successfully added a new role.')
        except:
            # in case role name already exists
            flash('Error: role name already exists.')

        # redirect to the roles page
        return redirect(url_for('admin.list_roles'))

    # load role template
    return render_template('admin/roles/role.html', add_role=add_role,
                           form=form, title='Add Role')

@admin.route('/roles/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_role(id):
    """
    Edit a role
    """
    check_admin()

    add_role = False

    role = Role.query.get_or_404(id)
    form = RoleForm(obj=role)
    if form.validate_on_submit():
        role.name = form.name.data
        role.description = form.description.data
        db.session.add(role)
        db.session.commit()
        flash('You have successfully edited the role.')

        # redirect to the roles page
        return redirect(url_for('admin.list_roles'))

    form.description.data = role.description
    form.name.data = role.name
    return render_template('admin/roles/role.html', add_role=add_role,
                           form=form, title="Edit Role")

@admin.route('/roles/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_role(id):
    """
    Delete a role from the database
    """
    check_admin()

    role = Role.query.get_or_404(id)
    db.session.delete(role)
    db.session.commit()
    flash('You have successfully deleted the role.')

    # redirect to the roles page
    return redirect(url_for('admin.list_roles'))

    return render_template(title="Delete Role")



# Employee Views

@admin.route('/employees')
@login_required
def list_employees():
    """
    List all employees
    """
    check_admin()

    employees = Employee.query.all()
    return render_template('admin/employees/employees.html',
                           employees=employees, title='Employees')

@admin.route('/employees/assign/<int:id>', methods=['GET', 'POST'])
@login_required
def assign_employee(id):
    """
    Assign a department and a role to an employee
    """
    check_admin()

    employee = Employee.query.get_or_404(id)

    # prevent admin from being assigned a department or role
    if employee.is_admin:
        abort(403)

    form = EmployeeAssignForm(obj=employee)
    if form.validate_on_submit():
        employee.department = form.department.data
        employee.role = form.role.data
        db.session.add(employee)
        db.session.commit()
        flash('You have successfully assigned a department and role.')

        # redirect to the roles page
        return redirect(url_for('admin.list_employees'))

    return render_template('admin/employees/employee.html',
                           employee=employee, form=form,
                           title='Assign Employee')


# Images Views

@admin.route('/images')
@login_required
def list_images():
    check_admin()
    """
    List all images
    """
    images = BImage.query.all()
    return render_template('admin/images/images.html',
                           images=images, title='Images')


@admin.route('/images/add', methods=['GET', 'POST'])
@login_required
def add_image():
    """
    Add a image to the database
    """
    check_admin()

    add_role = True

    form = ImageForm()
    if form.validate_on_submit():
        
        bimg_name=('C:/ImageBanner/'+strip(form.name.data)+'.jpg')
        bimg_open= open(bimg_name)
        bimg_read=bimg_open.read()
        bimg_open.close()
        bimg_bin = escape_string(bimg_read)
        bimg_banner = form.is_banner.data

        img_banner = BImage(im_banner=bimg_bin,
                            is_banner=bimg_banner,
                            name=strip(form.name.data))

        
        if bimg_banner:
            images_all = BImage.query.all()
            actualiza_imagen(images_all,bimg_name)

        try:
            # add image to the database 
            db.session.add(img_banner)
            db.session.commit()

            flash('You have successfully added a new image.')
        except:
            # in case role name already exists           
            flash('Error: image name already exists.')

        # redirect to the images page
        return redirect(url_for('admin.list_images'))
            
    # load image template
    return render_template('admin/images/image.html', add_role=add_role,
                           form=form, title='Add Image')
    
    
@admin.route('/images/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_image(id):
    """
    Edit a image
    """
    check_admin()

    add_role = False
    
    img_banner = BImage.query.get_or_404(id)
    form = ImageForm(obj=img_banner)
    if form.validate_on_submit():
        
        bimg_name=('C:/ImageBanner/'+strip(form.name.data)+'.jpg')
        bimg_banner = form.is_banner.data
        if bimg_banner:
           images_all = BImage.query.all()
           actualiza_imagen(images_all,bimg_name)
                
        img_banner.is_banner = form.is_banner.data
        db.session.add(img_banner)
        db.session.commit()
        flash('You have successfully edited the image.')

        # redirect to the roles page
        return redirect(url_for('admin.list_images'))
    
    form.name.data = img_banner.name
    form.is_banner.data = img_banner.is_banner
    return render_template('admin/images/image.html', add_role=add_role,
                           form=form, title="Edit Image")

  
@admin.route('/images/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_image(id):
    """
    Delete a image from the database
    """
    check_admin()

    img_banner = BImage.query.get_or_404(id)
    db.session.delete(img_banner)
    db.session.commit()
    flash('You have successfully deleted the image.')

    # redirect to the roles page
    return redirect(url_for('admin.list_images'))

    return render_template(title="Delete Image")